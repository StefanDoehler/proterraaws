"""
This file is specific to AWS, but its interface can be
replicated for other cloud providers
"""

from data_store import DataPoint, get_datastore, get_atomic_counter, CounterTable, delete_ac, delete_ds
from subprocess import Popen
import os
import time

PROCESS = None

def data_store_set_up():
    """
    Use DynamoDB local, which runs on port 8000 by default
    Install JRE 7 and DynamoDB Local
    Unzip/tar the contents of DynamoDB Local to a directory named DynamoDBLocal
    """
    DataPoint.Meta.host = "http://localhost:8000"
    CounterTable.Meta.host = "http://localhost:8000"
    get_datastore()
    get_atomic_counter()


def data_store_tear_down():
    """
    Delete the tables
    """
    delete_ac()
    delete_ds()


def connect_dynamo_local():
    global PROCESS
    # modify this path to the directory of DynamoDBLocal
    dynamo_local_path = "/home/vagrant/Dev/proterra-flask/DynamoDBLocal"
    com = ['java', '-Djava.library.path=./DynamoDBLocal_lib', '-jar', 'DynamoDBLocal.jar']

    try:
        os.chdir(dynamo_local_path)
    except OSError:
        raise OSError("Make sure to modify dynamo_local_path correctly")

    PROCESS = Popen(com, stdout=None, stderr=None, stdin=None)
    time.sleep(3)  # wait for connection to DynamoDBLocal


def disconnect_dynamo_local():
    global PROCESS
    PROCESS.terminate()
