from unittest import TestCase, main
from datetime import datetime, timedelta
import pytz
import json
import webtest

from data_store_test_setup import data_store_set_up, data_store_tear_down, connect_dynamo_local, disconnect_dynamo_local
from data_store import DataPoint, get_atomic_counter, get_datastore

from jose import jws

import application


class TestDataStore(TestCase):

    @classmethod
    def setUpClass(cls):
        connect_dynamo_local()

    @classmethod
    def tearDownClass(cls):
        disconnect_dynamo_local()

    def setUp(self):
        data_store_set_up()

    def tearDown(self):
        data_store_tear_down()

    def test_atomic_counter(self):
        ac = get_atomic_counter()
        self.assertEqual(ac.get_count(), 0)
        self.assertEqual(ac.store_count(), 0)
        self.assertEqual(ac.get_count(), 1)
        ac.increment_count()
        ac.increment_count()
        self.assertEqual(ac.get_count(), 3)
        self.assertEqual(ac.store_count(), 3)
        self.assertEqual(ac.store_count(), 4)
        self.assertEqual(ac.get_count(), 5)

    def test_last_point_empty(self):
        ds = get_datastore()
        lp = ds.last_point()
        self.assertIsNone(lp.end_date_time)
        self.assertIsNone(lp.start_date_time)
        self.assertEqual(lp.update_id, -1)
        self.assertEqual(lp.miles, 0)
        self.assertEqual(lp.energy, 0)
        self.assertEqual(ds.get_total_miles(), 0)
        self.assertEqual(ds.get_total_energy(), 0)

    def test_one_datapoint_entry(self):
        self.test_last_point_empty()

        ds = get_datastore()
        ac = get_atomic_counter()

        start_date_time = datetime.now()
        end_date_time = start_date_time + timedelta(0, 1)
        ds.add_point(DataPoint(start_date_time=start_date_time, start_date_time_copy=start_date_time,
                               end_date_time=end_date_time, energy=0, miles=0,
                               total_miles=ds.get_total_miles(), total_energy=ds.get_total_energy(),
                               update_id=ac.store_count()))

        self.assertEqual(ds.last_point().start_date_time, pytz.utc.localize(start_date_time))
        self.assertEqual(ds.last_point().start_date_time_copy, pytz.utc.localize(start_date_time))
        self.assertEqual(ds.last_point().end_date_time, pytz.utc.localize(end_date_time))
        self.assertEqual(ds.last_point().energy, 0)
        self.assertEqual(ds.last_point().miles, 0)
        self.assertEqual(ds.last_point().update_id, 0)

        self.assertEqual(ds.first_point().start_date_time, pytz.utc.localize(start_date_time))
        self.assertEqual(ds.first_point().start_date_time_copy, pytz.utc.localize(start_date_time))
        self.assertEqual(ds.first_point().end_date_time, pytz.utc.localize(end_date_time))
        self.assertEqual(ds.first_point().energy, 0)
        self.assertEqual(ds.first_point().miles, 0)
        self.assertEqual(ds.first_point().update_id, 0)

        self.assertEqual(ds.get_total_energy(), 0)
        self.assertEqual(ds.get_total_miles(), 0)

    def test_two_datapoint_entries(self):
        self.test_last_point_empty()

        ds = get_datastore()
        ac = get_atomic_counter()

        start_date_time = datetime.now()
        end_date_time = start_date_time + timedelta(0, 1)
        ds.add_point(DataPoint(start_date_time=start_date_time, start_date_time_copy=start_date_time,
                               end_date_time=end_date_time, energy=10.2, miles=20.6,
                               total_miles=ds.get_total_miles(20.6), total_energy=ds.get_total_energy(10.2),
                               update_id=ac.store_count()))

        ds.add_point(DataPoint(start_date_time=end_date_time, start_date_time_copy=end_date_time,
                               end_date_time=(end_date_time + timedelta(0, 1)), energy=253.9, miles=612.424,
                               total_miles=ds.get_total_miles(612.424), total_energy=ds.get_total_energy(253.9),
                               update_id=ac.store_count()))

        self.assertEqual(ds.last_point().start_date_time, ds.second_point().start_date_time)
        self.assertEqual(ds.last_point().start_date_time_copy, ds.second_point().start_date_time_copy)
        self.assertEqual(ds.last_point().end_date_time, ds.second_point().end_date_time)
        self.assertEqual(ds.last_point().energy, ds.second_point().energy)
        self.assertEqual(ds.last_point().miles, ds.second_point().miles)
        self.assertEqual(ds.last_point().update_id, ds.second_point().update_id)

        self.assertEqual(ds.last_point().start_date_time, pytz.utc.localize(end_date_time))
        self.assertEqual(ds.last_point().start_date_time_copy, pytz.utc.localize(end_date_time))
        self.assertEqual(ds.last_point().end_date_time, pytz.utc.localize(end_date_time + timedelta(0, 1)))
        self.assertEqual(ds.last_point().energy, 253.9)
        self.assertEqual(ds.last_point().miles, 612.424)
        self.assertEqual(ds.last_point().update_id, 1)

        self.assertEqual(ds.first_point().start_date_time, pytz.utc.localize(start_date_time))
        self.assertEqual(ds.first_point().start_date_time_copy, pytz.utc.localize(start_date_time))
        self.assertEqual(ds.first_point().end_date_time, pytz.utc.localize(end_date_time))
        self.assertEqual(ds.first_point().energy, 10.2)
        self.assertEqual(ds.first_point().miles, 20.6)
        self.assertEqual(ds.first_point().update_id, 0)

        self.assertEqual(ds.get_total_energy(), 264.1)
        self.assertEqual(ds.get_total_miles(), 633.024)

    # def get_data_points_in_range(cls, min_start, max_start):
    def test_get_data_points_in_range(self):
        self.test_last_point_empty()

        ds = get_datastore()
        ac = get_atomic_counter()

        now = datetime.now()
        start_date_time = now
        end_date_time = start_date_time + timedelta(0, 1)
        point1 = DataPoint(start_date_time=start_date_time, start_date_time_copy=start_date_time,
                           end_date_time=end_date_time, energy=10.2, miles=20.6,
                           total_miles=ds.get_total_miles(20.6), total_energy=ds.get_total_energy(10.2),
                           update_id=ac.store_count())

        point2 = DataPoint(start_date_time=end_date_time, start_date_time_copy=end_date_time,
                           end_date_time=(end_date_time+timedelta(0, 1)), energy=253.9, miles=612.424,
                           total_miles=ds.get_total_miles(612.424), total_energy=ds.get_total_energy(253.9),
                           update_id=ac.store_count())
        ds.add_point(point1)
        ds.add_point(point2)

        points = ds.get_data_points_in_range(start_date_time, end_date_time)
        self.assertEqual(len(points), 2)
        # scan returns items in random sort, so we cannot be sure point1 is the first item
        point1bool = (points[0].miles == 20.6 and points[1].miles != 20.6) or \
                     (points[0].miles != 20.6 and points[1].miles == 20.6)
        point2bool = (points[0].energy == 253.9 and points[1].energy != 253.9) or \
                     (points[0].energy != 253.9 and points[1].energy == 253.9)
        self.assertTrue(point1bool)
        self.assertTrue(point2bool)

        points = ds.get_data_points_in_range(start_date_time, start_date_time)
        self.assertEqual(len(points), 1)
        self.assertEqual(points[0].miles, 20.6)

        points = ds.get_data_points_in_range(end_date_time, end_date_time)
        self.assertEqual(len(points), 1)
        self.assertEqual(points[0].miles, 612.424)

        points = ds.get_data_points_in_range(end_date_time+timedelta(0, 4), end_date_time+timedelta(0, 5))
        self.assertEqual(len(points), 0)

    def test_jws_secret(self):
        ds = get_datastore()
        ds.set_jws_secret('hello')
        self.assertEqual('hello', ds.get_jws_secret())

        # setting the secret in rapid succession will sometimes overload the throughput,
        # causing an error; just run again
        ds.set_jws_secret('hello2')
        ds.set_jws_secret('hello3')
        ds.set_jws_secret('hello4')
        self.assertEqual('hello4', ds.get_jws_secret())


class TestPortal(TestCase):

    def create_auth_payload(self, payload):
        secret = get_datastore().get_jws_secret()
        signed = jws.sign(payload, secret, algorithm='HS256')
        return str(signed)

    def post_update_authenticated_json(self, payload, **kwargs):
        auth_payload = self.create_auth_payload(payload)
        response = self.testapp.post('/KCM/api/v1/update', auth_payload, **kwargs)
        return response

    @classmethod
    def setUpClass(cls):
        connect_dynamo_local()

    @classmethod
    def tearDownClass(cls):
        disconnect_dynamo_local()

    def setUp(self):
        self.testapp = webtest.TestApp(application.application)
        data_store_set_up()
        get_datastore().set_jws_secret('MY_SECRET')

    def tearDown(self):
        data_store_tear_down()

    def test_simple_response(self):
        response = self.testapp.get('/KCM/')
        self.assertTrue('text/html' in response.headers['Content-Type'])

    def test_get_update_returns_405(self):
        self.testapp.get('/KCM/api/v1/update', status=405)

    def test_last_update_no_data_succeeds(self):
        response = self.testapp.get('/KCM/api/v1/last_updated')
        self.assertEqual(response.status_int, 200)
        self.assertTrue('application/json' in response.headers['Content-Type'])
        last_updated = json.loads(response.body)

        self.assertEqual(last_updated['end_time'], 0)
        self.assertEqual(last_updated['id'], -1)

    def test_updates_and_match_last_updated(self):
        payload = {'start_time': 1454115123, 'end_time': 1454117375, 'data': {'energy': 12345.6789, 'miles': 5684.25}}
        response = self.post_update_authenticated_json(payload)
        self.assertEqual(response.status_int, 200)

        response = self.testapp.get('/KCM/api/v1/last_updated')
        self.assertEqual(response.status_int, 200)
        self.assertTrue('application/json' in response.headers['Content-Type'])
        last_updated = json.loads(response.body)

        self.assertEqual(last_updated['end_time'], 1454117375)
        self.assertEqual(last_updated['id'], 0)

        response = self.post_update_authenticated_json({'start_time': 1454117375, 'end_time': 1454121128,
                                                        'data': {'energy': 3456, 'miles': 12}})
        self.assertEqual(response.status_int, 200)

        response = self.testapp.get('/KCM/api/v1/last_updated')
        self.assertEqual(response.status_int, 200)
        self.assertTrue('application/json' in response.headers['Content-Type'])
        last_updated = json.loads(response.body)

        self.assertEqual(last_updated['end_time'], 1454121128)
        self.assertEqual(last_updated['id'], 1)

        ds = get_datastore()
        self.assertAlmostEqual(ds.get_total_energy(), 12345.6789 + 3456)
        self.assertAlmostEqual(ds.get_total_miles(), 5684.25 + 12)

    def test_update_with_missing_miles_data(self):
        self.post_update_authenticated_json({'start_time': 1454115123, 'end_time': 1454117375,
                                             'data': {'energy': 2357.4358}})
        ds = get_datastore()
        self.assertAlmostEqual(ds.get_total_miles(), 0)
        self.assertAlmostEqual(ds.get_total_energy(), 2357.4358)
        self.assertAlmostEqual(ds.last_point().miles, 0)
        self.assertAlmostEqual(ds.last_point().energy, 2357.4358)

    def test_update_with_correct_start_time_succeeds(self):
        self.post_update_authenticated_json({'start_time': 1454115123, 'end_time': 1454117375, 'data': {}})
        self.post_update_authenticated_json({'start_time': 1454117375, 'end_time': 1454121128, 'data': {}})

    def test_update_with_earlier_start_time_fails(self):
        self.post_update_authenticated_json({'start_time': 1454115123, 'end_time': 1454117375, 'data': {}})
        self.post_update_authenticated_json({'start_time': 1454117374, 'end_time': 1454121128, 'data': {}}, status=400)

    def test_update_with_later_start_time_fails(self):
        self.post_update_authenticated_json({'start_time': 1454115123, 'end_time': 1454117375, 'data': {}})
        self.post_update_authenticated_json({'start_time': 1454117376, 'end_time': 1454121128, 'data': {}}, status=400)

    def test_update_with_missing_field_fails(self):
        self.post_update_authenticated_json({'start_time': 1454115123, 'end_time': 1454117375}, status=400)
        self.post_update_authenticated_json({'end_time': 1454117375, 'data': {}}, status=400)
        self.post_update_authenticated_json({'start_time': 1454115123, 'data': {}}, status=400)

    def test_bad_auth_secret_fails(self):
        secret = 'NOT_THE_SECRET'
        payload = {'start_time': 1454115123, 'end_time': 1454117375, 'data': {}}
        bad_auth_payload = str(jws.sign(payload, secret, algorithm='HS256'))
        self.testapp.post('/KCM/api/v1/update', bad_auth_payload, status=401)

if __name__ == "__main__":
    main()
