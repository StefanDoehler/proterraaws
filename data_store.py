from pynamodb.models import Model
from pynamodb.indexes import GlobalSecondaryIndex, AllProjection
from pynamodb.attributes import NumberAttribute, UTCDateTimeAttribute, UnicodeAttribute
from pynamodb.exceptions import DoesNotExist, TableDoesNotExist

DATA_STORE = None
ATOMIC_COUNTER = None


class CounterTable(Model):
    """
    The table that stores the atomic counter in DynamoDB
    """
    class Meta:
        table_name = 'Counter'

    key = UnicodeAttribute(hash_key=True)
    count = NumberAttribute()


class _AtomicCounter(object):
    """
    Allows interaction with CounterTable in order to update the atomic counter
    """
    def __init__(self):
        if not CounterTable.exists():
            CounterTable.create_table(read_capacity_units=1, write_capacity_units=1, wait=True)
            counter = CounterTable(hash_key='key', count=0)
            counter.save()

    @classmethod
    def get_count(cls):
        """
        :return: the current count
        """
        return CounterTable.get('key').count

    @classmethod
    def increment_count(cls):
        """
        Increment the count
        """
        c = CounterTable.get('key')
        c.update_item('count', 1, action='add')

    @classmethod
    def store_count(cls):
        """
        Return the current count and then increment the count
        :return: current count (before incrementation)
        """
        c = cls.get_count()
        cls.increment_count()
        return c


def get_atomic_counter():
    """
    Use this to get the _AtomicCounter instance
    :rtype: _AtomicCounter
    """
    global ATOMIC_COUNTER
    if ATOMIC_COUNTER is None:
        ATOMIC_COUNTER = _AtomicCounter()
    return ATOMIC_COUNTER


def get_datastore():
    """
    Use this to get the _DataStore instance
    :rtype: _DataStore
    """
    global DATA_STORE
    if DATA_STORE is None:
        DATA_STORE = _DataStore()
    return DATA_STORE


class StartDateIndex(GlobalSecondaryIndex):
    """
    Allows start_date_time to be indexed
    """
    class Meta:
        read_capacity_units = 1
        write_capacity_units = 1
        # all attributes are projected
        projection = AllProjection()

    start_date_time = UTCDateTimeAttribute(hash_key=True)


class DataPoint(Model):
    """
    A single DataPoint with miles and energy
    """
    # a meta class is required by PynamoDB with at least the table name
    class Meta:
        table_name = 'DataStore'

    miles = NumberAttribute(default=0)
    energy = NumberAttribute(default=0)
    total_miles = NumberAttribute(default=0)
    total_energy = NumberAttribute(default=0)

    start_date_index = StartDateIndex()
    start_date_time = UTCDateTimeAttribute(null=True)
    start_date_time_copy = UTCDateTimeAttribute()
    end_date_time = UTCDateTimeAttribute(null=True)

    update_id = NumberAttribute(hash_key=True, default=-1)

    def __str__(self):
        return str({"update_id:": self.update_id, "start_date_time": self.start_date_time,
                    "start_date_time_copy": self.start_date_time_copy, "end_date_time": self.end_date_time,
                    "total_miles": self.total_miles, "total_energy": self.total_energy,
                    "miles": self.miles, "energy": self.energy})


class JWSSecret(Model):
    class Meta:
        table_name = 'JWSSecret'

    key = UnicodeAttribute(hash_key=True)
    secret = UnicodeAttribute(null=True)


class _DataStore(object):
    """
    The DataStore singleton.
    DO NOT construct one of these manually. Use get_datastore() instead
    """

    def __init__(self):
        if not DataPoint.exists():
            DataPoint.create_table(read_capacity_units=1, write_capacity_units=1, wait=True)

    @classmethod
    def get_total_miles(cls, miles=0):
        """
        :return: The total amount of miles from the last_point() added to miles
        """
        return float(cls.last_point().total_miles) + miles

    @classmethod
    def get_total_energy(cls, energy=0):
        """
        :return: The total amount of energy from the last_point() added to energy
        """
        return float(cls.last_point().total_energy) + energy

    @classmethod
    def add_point(cls, point):
        """
        :type point DataPoint
        """
        point.save()

    @classmethod
    def first_point(cls):
        """
        :rtype DataPoint
        """
        try:
            return DataPoint.get(hash_key=0)
        except DoesNotExist:
            return DataPoint()

    @classmethod
    def second_point(cls):
        """
        :rtype DataPoint
        """
        try:
            return DataPoint.get(hash_key=1)
        except DoesNotExist:
            return DataPoint()

    @classmethod
    def last_point(cls):
        """
        :rtype DataPoint
        """
        index = get_atomic_counter().get_count()-1
        try:
            return DataPoint.get(hash_key=index)
        except DoesNotExist:
            return DataPoint()

    @classmethod
    def get_data_points_in_range(cls, min_start, max_start):
        """
        :param min_start: minimum start_date_time
        :param max_start: maximum start_date_time_copy
        :rtype list of DataPoints
        """
        # PynamoDB's scan takes care of paginating for us
        # NOTE that scan returns the items in random order
        return [p for p in DataPoint.scan(start_date_time__ge=min_start, start_date_time_copy__le=max_start)]


    @classmethod
    def get_jws_secret(cls):
        """
        :rtype string secret
        """
        try:
            return JWSSecret.get(hash_key='key').secret
        except (DoesNotExist, TableDoesNotExist):
            return None

    @classmethod
    def set_jws_secret(cls, secret):
        """
        Sets the JWSSecret to secret, overwriting the old secret if it exists
        :param secret: a new secret
        """
        if not JWSSecret.exists():
            JWSSecret.create_table(read_capacity_units=1, write_capacity_units=1, wait=True)

        s = JWSSecret('key')
        s.update_item('secret', secret, action='put')


def delete_ds():
    global DATA_STORE
    if DataPoint.exists():
        DATA_STORE = None
        DataPoint.delete_table()


def delete_ac():
    global ATOMIC_COUNTER
    if CounterTable.exists():
        ATOMIC_COUNTER = None
        CounterTable.delete_table()






