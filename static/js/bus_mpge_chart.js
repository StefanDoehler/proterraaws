
(function() {

	"use strict";
    var mpge_chart_data = JSON.parse(document.getElementById("mpge_chart_data").innerHTML);

    var chartData = {
        "type":"bar",
        "series":[
            { "values": mpge_chart_data.fleet_miles_data,
              "background-color": "#006b54 #00bb90",
              "text": "Miles Driven"
            },
            {
                "values": mpge_chart_data.fleet_diesel_gals_saved_data,
                "background-color": "#61a813 #b9e023",
                "text": "Diesel Gals Saved"
            }
        ],
        "title": {
            "text": "Monthly Fleet Miles\nand Diesel Gallons Saved",
            "color": "#006b54",
            "font": "Arial",
            "font-size": 16,
            "offset-y": 0
        },
        "plotarea": {
            "background-color": "#e0e0e0"
        },
        "legend": {
            "align": "center",
            "vertical-align": "top",
            "offset-y": 50
        },
        "scale-y":{
            //"values":"0:10000:1000",
            "line-color":"#555",
            "line-width":"2px",
            "guide":{
                "line-color": "#999999",
                "line-width": "1px",
                "line-style": "solid"
            },
            "tick":{
                "size":5,
                "line-width":2,
                "line-color":"#555",
                "placement":"outer"
            },
            "item":{
                "padding":"5px"
            }
        },
        "scale-x": {
            "values": mpge_chart_data.labels,
            "line-color":"#555",
            "line-width":"2px",
            "guide":{
                "line-color":"#555",
                "line-width":"1px"
            },
            "item":{
                "padding":"0px"
            }
        }
    };

    zingchart.render({ // Render Method[3]
        id:'chart_bus_mpge_div',
        data:chartData,
        height:450,
        width:"100%",
        "auto-resize": true
    });
})();

