from flask import Flask, render_template, make_response, request
from flask.views import MethodView
from werkzeug.contrib.cache import SimpleCache

from datetime import date, time, datetime, timedelta
import calendar
import pytz

import json
from jose import jws
from jose.exceptions import JWSError

from data_store import get_datastore, get_atomic_counter

from data_store import DataPoint

# conversion factors
KWH_PER_GAL_DIESEL = 37.95  # from https://en.wikipedia.org/wiki/Gasoline_gallon_equivalent
DIESEL_BUS_AVG_MPG = 6.3  # from customer email
LBS_CO2E_PER_DIESEL_GAL = 22.4  # from customer email

# create the flask application
application = Flask(__name__)

# set up caching through werkzeug
cache = SimpleCache()
cache_timeout = 60 * 60  # 1 hour
cache_key = "main_page"


class MainPage(MethodView):
    """
    The main page that will read data from the data store and display it
    """
    @classmethod
    def digit_list(cls, number):
        """
        Converts input number (can be a float) to an integer, then a list of digits separated by commas every 1000

        Example: digit_list(123456.46) == ['1', '2', '3', ',', '4', '5', '6']
        """
        str_with_commas = "{:,}".format(int(number))
        return [x for x in str_with_commas]

    @classmethod
    def calc_efficiency_values(cls, miles):
        diesel_gals_saved = 0
        co2e_reduced = 0

        if miles > 0:
            diesel_gals_saved = miles / DIESEL_BUS_AVG_MPG
            co2e_reduced = diesel_gals_saved * LBS_CO2E_PER_DIESEL_GAL

        return diesel_gals_saved, co2e_reduced

    @classmethod
    def create_mpge_chart_json_data(cls):

        month_label_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

        # Create list of datetime pairs that start and end each of the prior 11 months
        now = datetime.now()
        month = now.month + 1
        year = now.year - 1

        # data starts in January 2016, so that is the minimum start date
        if year < 2016:
            year = 2016
            month = 1

        time_pairs = []
        labels = []
        for _ in xrange(11):
            start_date = date(year=year, month=month, day=1)
            start_date_time = datetime.combine(start_date, time.min)

            labels.append(month_label_names[month - 1])

            month += 1
            if month > 12:
                month = 1
                year += 1

            end_date = date(year=year, month=month, day=1) - timedelta(days=1)
            end_date_time = datetime.combine(end_date, time.max)

            time_pairs.append((start_date_time, end_date_time))

        # append pair that contains start of this month up to right now
        start_date = date(year=year, month=month, day=1)
        start_date_time = datetime.combine(start_date, time.min)
        time_pairs.append((start_date_time, now))
        labels.append(month_label_names[month - 1])

        # get data from data store
        ds = get_datastore()

        fleet_miles_data = []
        fleet_diesel_gals_saved_data = []
        for pair in time_pairs:
            points = ds.get_data_points_in_range(pair[0], pair[1])
            fleet_miles_data.append(sum(x.miles for x in points))
            fleet_diesel_gals_saved_data.append(sum(cls.calc_efficiency_values(x.miles)[0] for x in points))

        data = {"labels": labels,
                "fleet_miles_data": fleet_miles_data,
                "fleet_diesel_gals_saved_data": fleet_diesel_gals_saved_data}

        return json.dumps(data)

    @classmethod
    def generate_main_page_html(cls):
        ds = get_datastore()
        miles = ds.get_total_miles()
        energy = ds.get_total_energy()
        diesel_gals_saved, co2e_reduced = cls.calc_efficiency_values(miles)

        last_updated = "--"
        in_service_since = "__"
        end_date_time = ds.last_point().end_date_time
        if end_date_time is not None:
            last_updated = end_date_time.strftime("%m/%d/%Y")

        ####################
        # NOTE: customer data population incorrectly started at time 0, so
        # we are ignoring the first point and returning the second point

        # start_date_time = ds.first_point.start_date_time
        start_date_time = ds.second_point().start_date_time
        ###################

        if start_date_time is not None:
            in_service_since = start_date_time.strftime("%m/%d/%Y")

        mpge_chart_data = cls.create_mpge_chart_json_data()

        template = render_template('index.html',
                                   last_updated=last_updated,
                                   in_service_since=in_service_since,
                                   total_miles_digits=cls.digit_list(miles),
                                   total_kwh_digits=cls.digit_list(energy),
                                   diesel_gals_saved_digits=cls.digit_list(diesel_gals_saved),
                                   co2e_reduced_digits=cls.digit_list(co2e_reduced),
                                   mpge_chart_data=mpge_chart_data)

        response = make_response(template)
        response.headers['Content-Type'] = 'text/html'
        cache.set(cache_key, response, timeout=cache_timeout)
        return response

    def get(self):
        cached_page = cache.get(cache_key)
        if cached_page is None:
            return self.generate_main_page_html()
        return cached_page


class UpdaterAPIPage(MethodView):
    """
    Secured JSON page that will allow API update access to the datastore
    """

    def get(self):
        # Only POST is allowed to update
        return make_response(("GET not allowed", 405))

    def post(self):
        secret = get_datastore().get_jws_secret()
        if secret is None:
            return make_response(("Authentication not available", 503))
        try:
            auth_payload = request.form.keys()[0]
            json_obj = jws.verify(auth_payload, secret, algorithms=['HS256'])

        except JWSError:
            return make_response(("Authentication failed", 401))

        try:
            params = [json_obj.get(x, None) for x in ("start_time", "end_time", "data")]

            if None in params:
                msg = {'id': get_datastore().last_point().update_id, 'status': "Missing required parameter"}
                response = make_response((msg, 400))
                response.headers['Content-Type'] = 'application/json'
                return response

            self.new_point(*params)

        except Exception as e:
            msg = {'id': get_datastore().last_point().update_id, 'status': str(e)}
            response = make_response((json.dumps(msg), 400))
            response.headers['Content-Type'] = 'application/json'
            return response

        MainPage.generate_main_page_html()

        msg = json.dumps({'id': get_datastore().last_point().update_id, 'status': 'ok'})
        response = make_response((msg, 200))
        response.headers['Content-Type'] = 'application/json'
        return response

    @classmethod
    def new_point(cls, start_date_time, end_date_time, data):
        """
        Creates a new DataPoint from the provided data and adds it to the data store.

        :param start_date_time: seconds since epoch in UTC
        :param end_date_time: seconds since epoch in UTC
        :param data: dictionary of data
        :return: none
        """

        ds = get_datastore()
        ac = get_atomic_counter()
        lp = ds.last_point()

        start_date_time = datetime.utcfromtimestamp(start_date_time)
        end_date_time = datetime.utcfromtimestamp(end_date_time)

        if not (pytz.utc.localize(start_date_time) == lp.end_date_time or lp.end_date_time is None):
            raise ValueError("Error: start_date_time not equal to last updated end_date_time")

        miles = data.get("miles", 0)
        energy = data.get("energy", 0)
        ds.add_point(DataPoint(start_date_time=start_date_time, start_date_time_copy=start_date_time,
                               end_date_time=end_date_time, energy=energy, miles=miles,
                               total_miles=ds.get_total_miles(miles), total_energy=ds.get_total_energy(energy),
                               update_id=ac.store_count()))


class ReaderAPIPage(MethodView):
    """
    Secured JSON page that will allow API READ access
    """

    def get(self):
        d = {"id": -1, "end_time": 0}
        ds = get_datastore()
        lp = ds.last_point()

        if lp.end_date_time is not None:
            d = {"id": lp.update_id, "end_time": calendar.timegm(lp.end_date_time.timetuple())}

        last_point_json = json.dumps(d)
        response = make_response(last_point_json)
        response.headers['Content-Type'] = 'application/json'
        return response


application.add_url_rule('/KCM/', view_func=MainPage.as_view('main_page'))
application.add_url_rule('/KCM/api/v1/update', view_func=UpdaterAPIPage.as_view('updater_api_page'))
application.add_url_rule('/KCM/api/v1/last_updated', view_func=ReaderAPIPage.as_view('reader_api_page'))

if __name__ == "__main__":
    application.run()
